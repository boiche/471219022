#!/bin/bash

set -e
export PROJECT_ID=$(gcloud config get-value core/project)
export UNIQUE_IDENTIFIER='<gitlab group>-<gitlab-project-022>'
export NAMESPACE="gitlab-022"
export SERVICE_ACCOUNT_NAME="gitlab-runner-022";
export SERVICE_ACCOUNT_EMAIL="${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com" # "gitlab-runner@tu-sofia-pis-2022-dev.iam.gserviceaccount.com"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME} --display-name "GitLab Runner022" || true;

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
       --member="serviceAccount:${SERVICE_ACCOUNT_EMAIL}" \
       --role='roles/owner' || echo "Failed to give service account permissions (needs owner permissions), do this manually";

kubectl create namespace gitlab-022 || true

if kubectl describe secret google-key --namespace=gitlab-022 >/dev/null ; then
    echo -n "";
else
    gcloud iam service-accounts keys create \
       --iam-account "${SERVICE_ACCOUNT_EMAIL}" \
       service-account.json

    kubectl create secret generic google-key --from-file service-account.json --namespace=gitlab-022
fi;

if kubectl get clusterrolebinding gitlab-022-cluster-admin; then
    echo -n ""
else
    kubectl create clusterrolebinding gitlab-022-cluster-admin --clusterrole=cluster-admin --serviceaccount=gitlab-022:default
fi;
